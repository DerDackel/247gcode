import {TestPlan, writePrinterSetup} from "../src/components/speedtest-generator";

const defaultValues: TestPlan = {
    printer: {
        name: "247Zero Beta1",
        kinematics: 'CoreXY',
    },
    iterations: 1,
    speed: {
        start: 100,
        steps: 3,
        increment: 50,
        scv: 5,
    },
    acceleration: {
        start: 1000,
        steps: 3,
        increment: 1000,
    },
    bed: {
        sizeX: 120,
        sizeY: 120,
        safetyMargin: 10,
    }
};
describe('The Printer Setup section', () => {
    test('should leave newline at its end', () => {
        const result = writePrinterSetup(defaultValues);
        console.log(result);
        expect(result.endsWith('\n\n')).toBeTruthy();
    });

});