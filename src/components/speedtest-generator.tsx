import styles from '../styles/SpeedTestGenerator.module.css';
import React from "react";
import {FormState, useForm, UseFormGetValues, UseFormRegister} from "react-hook-form";
import {ErrorMessage} from "@hookform/error-message";

export interface Printer {
    name: string;
    kinematics?: "CoreXY" | "Cartesian" | "Delta";
}

export interface Bed {
    sizeX: number;
    sizeY: number;
    safetyMargin: number;
}

export interface TestPlan {
    printer: Printer;
    iterations: number;
    speed: SpeedPlan;
    acceleration: AccelerationPlan;
    bed: Bed;
}

export interface SpeedPlan {
    start: number;
    steps: number;
    increment: number;
    scv: number;
}

export interface AccelerationPlan {
    start: number;
    steps: number;
    increment: number;
}

type Point = { x: number; y: number };

export function SpeedtestGenerator() {
    const defaultValues: TestPlan = {
        printer: {
            name: "247Zero Beta1",
            kinematics: 'CoreXY',
        },
        iterations: 1,
        speed: {
            start: 100,
            steps: 3,
            increment: 50,
            scv: 5,
        },
        acceleration: {
            start: 1000,
            steps: 3,
            increment: 1000,
        },
        bed: {
            sizeX: 120,
            sizeY: 120,
            safetyMargin: 10,
        }
    };

    const form = useForm<TestPlan>({defaultValues, mode: "all", reValidateMode: 'onChange', criteriaMode: 'all'});

    return (<div>
        <form onSubmit={form.handleSubmit(handleGCode)} className={styles.generator}>
            <CommonSettings {...form}/>
            <BedSettings {...form}/>
            <SpeedSettings {...form}/>
            <AccelerationSettings {...form}/>
            <button className={styles.submit} type="submit">Generate!</button>
        </form>
    </div>);
}

interface FormProps {
    register: UseFormRegister<TestPlan>;
    getValues: UseFormGetValues<TestPlan>;

    formState: FormState<TestPlan>
}

const CommonSettings = ({register}: FormProps) => {
    return (<>
        <h3 className={styles.section}>Common Settings</h3>
        <label className={styles.attribute} htmlFor="printer.name">Printer Name</label>
        <input className={styles.entry} type="text" {...register('printer.name', {
            required: true,
        })}/>
        <label className={styles.attribute} htmlFor="iterations">Number of Iterations</label>
        <input className={styles.entry} type="number" {...register('iterations', {
            required: true,
            min: 1,
            valueAsNumber: true
        })}/>
        <label className={styles.attribute} htmlFor="printer.kinematics">Kinematics</label>
        <select defaultValue={'CoreXY'} {...register('printer.kinematics')} disabled>
            <option value="CoreXY">CoreXY</option>
            <option value="Cartesian">Cartesian</option>
            <option value="Delta">Delta</option>
        </select>
    </>);
}

const BedSettings = ({register, formState: {errors}}: FormProps) => {
    return <>
        <h3 className={styles.section}>Bed Layout</h3>
        <label className={styles.attribute} htmlFor="bed.sizeX">Bed Size X</label>
        <input className={styles.entry} type="number" min={0} {...register('bed.sizeX', {
            required: true,
            min: 50,
            valueAsNumber: true
        })}/>
        <label className={styles.attribute} htmlFor="bed.sizeY">Bed Size Y</label>
        <input className={styles.entry} type="number" min={0} {...register('bed.sizeY', {
            required: true,
            min: 50,
            valueAsNumber: true
        })}/>
        <label className={styles.attribute} htmlFor="bed.safetyMargin">Safety Margin</label>
            <input className={styles.entry} type="number" min={0} {...register('bed.safetyMargin', {
                    valueAsNumber: true,
                    min: {value: 0, message: "Safety margin can't be negative"},
                    validate: {
                        lessThanHalfOfBedX: (value, formValues) => {
                            if (value * 2 >= formValues.bed.sizeX) return 'Safety margin should be less than half of X'
                            else return true
                        },
                        lessThanHalfOfBedY: (value, formValues) => {
                            if (value * 2 >= formValues.bed.sizeY) return 'Safety margin should be less than half of Y'
                            else return true
                        },
                    }
                }
            )}/>
            {errors.bed?.safetyMargin &&
                <ErrorMessage
                    errors={errors}
                    name="bed.safetyMargin"
                    render={({messages}) => {
                        if (messages) return (
                            <ul className={styles.error}>{Object.keys(messages).map(key => <li className={styles.error}
                                                                                               key={key}>{messages[key]}</li>)}</ul>)
                        else return <div>YAY!</div>
                    }}
                />
            }
    </>;
}

const SpeedSettings = ({register}: FormProps) => {
    return (<>
        <h3 className={styles.section}>Speed settings</h3>
        <label className={styles.attribute} htmlFor="speed.start">Starting Speed</label>
        <input className={styles.entry} type="number" {...register('speed.start', {
            required: true,
            min: 1,
            valueAsNumber: true
        })}/>
        <label className={styles.attribute} htmlFor="speed.increment">Speed Step Increment</label>
        <input className={styles.entry} type="number" {...register('speed.increment', {
            required: true,
            min: 1,
            valueAsNumber: true
        })}/>
        <label className={styles.attribute} htmlFor="speed.steps">Number of Steps</label>
        <input className={styles.entry} type="number" {...register('speed.steps', {
            required: true,
            min: 1,
            valueAsNumber: true
        })}/>
        <label className={styles.attribute} htmlFor="speed.scv">Square Corner Velocity Limit</label>
        <input className={styles.entry} type="number" {...register('speed.scv', {
            required: true,
            min: 1,
            valueAsNumber: true
        })}/>
    </>);
}

function AccelerationSettings({register}: FormProps) {
    return (<>
        <h3 className={styles.section}>Acceleration settings</h3>
        <label className={styles.attribute} htmlFor="acceleration.start">Starting Acceleration</label>
        <input className={styles.entry} type="number" {...register('acceleration.start', {
            required: true,
            min: 1,
            valueAsNumber: true
        })}/>
        <label className={styles.attribute} htmlFor="acceleration.increment">Acceleration Step Increment</label>
        <input className={styles.entry} type="number" {...register('acceleration.increment', {
            required: true,
            min: 1
        })}/>
        <label className={styles.attribute} htmlFor="acceleration.steps">Number of Steps</label>
        <input className={styles.entry} type="number" {...register('acceleration.steps', {
            required: true,
            min: 1,
            valueAsNumber: true
        })}/>
    </>)
}

const handleGCode = (params: TestPlan) => {
    console.log(params);
    const testCode = generateGCode(params);
    const element = document.createElement('a');
    const file = new Blob([testCode], {type: 'text/plain'});
    element.href = URL.createObjectURL(file);
    element.download = `testplan-${params.printer.name}-${new Date().toISOString()}.gcode`;
    document.body.appendChild(element);
    element.click();
};

function generateGCode(data: TestPlan) {
    const points = createPoints(data.bed);

    let gCodeBuffer = '; 247printing Y-Acceleration test gcode generator, remixed by DerDackel\n\n';
    gCodeBuffer += writePrinterSetup(data);
    gCodeBuffer += '\n; Points:\n';
    gCodeBuffer += JSON.stringify(points, undefined, 2).replace(/^/gm, ';');
    gCodeBuffer += '\n';
    gCodeBuffer += initGCode(points);

    for (let speedIteration = 0; speedIteration < data.speed.steps; speedIteration++) {
        for (let accelIteration = 0; accelIteration < data.acceleration.steps; accelIteration++) {
            const speed = data.speed.start + speedIteration * data.speed.increment;
            const accel = data.acceleration.start + accelIteration * data.acceleration.increment;
            console.log({accel, speed});
            gCodeBuffer += testIteration(speed, accel, accel, data.speed.scv, points);
        }
    }

    return gCodeBuffer;
}

export const writePrinterSetup = (testplan: TestPlan): string => {
    let testDescription = `; Printer: ${testplan.printer.name}\n`;
    testDescription += `;Test Input:\n${JSON.stringify(testplan, undefined, 2)}
  `.trimEnd().replace(/\n/g, '\n;') + '\n\n';
    return testDescription;
}

export function createPoints(reference: Bed): Point[] {
    return [
        {
            x: reference.safetyMargin,
            y: reference.safetyMargin,
        },
        {
            x: reference.sizeX / 2,
            y: reference.safetyMargin,
        },
        {
            x: reference.sizeX - reference.safetyMargin,
            y: reference.safetyMargin,
        },
        {
            x: reference.safetyMargin,
            y: reference.sizeY - reference.safetyMargin,
        },
        {
            x: reference.sizeX / 2,
            y: reference.sizeY - reference.safetyMargin,
        },
        {
            x: reference.sizeX - reference.safetyMargin,
            y: reference.sizeY - reference.safetyMargin,
        },
    ];
}

function initGCode(points: Point[]) {
    return `
  G90 ; absolute positioning
  G28 ; Home all axes
  M400
  M117 Moving to start position
  G1 Z20 F1800 ; Move nozzle away from bed
  ${moveXY(points[0].x, points[0].y, 50)}`.replace(/  +/g, '');
}

function testIteration(speed: number, accel: number, accToDec: number, scvSpeed: number, points: Point[]): string {
    return `
  M400 ; Make sure to finish all moves between iterations
  SET_VELOCITY_LIMIT ACCEL_TO_DECEL=${accToDec} ACCEL=${accel} VELOCITY=${speed} SQUARE_CORNER_VELOCITY=${scvSpeed}
  M117 ${speed}mm/s at ${accel}mm/s^2
  ${times(5, moveXY(points[5].x, points[5].y, speed) + moveXY(points[0].x, points[0].y, speed))}
  ${times(5, moveXY(points[2].x, points[2].y, speed) + moveXY(points[3].x, points[3].y, speed) + moveXY(points[2].x, points[2].y, speed))}
  ${times(5, moveXY(points[1].x, points[1].y, speed) + moveXY(points[4].x, points[4].y, speed) + moveXY(points[1].x, points[1].y, speed))}
  `.replace(/  +/g, '');
}

function moveXY(x: number, y: number, speed: number): string {
    return `G0 X${x} Y${y} F${speed * 60} ; move to (${x}, ${x} at ${(speed).toFixed(2)}mm/s\n`;
}

function times(n: number, s: string): string {
    return [...Array(n)].map(_ => s).join('\n');
}