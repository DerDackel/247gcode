import Image from "next/image";
import React from "react";
import styles from '../styles/Header.module.css';
import profilePic from "../../public/247coding.png";

interface HeaderProps {
    className: string;
}

export const Header = ({className}: HeaderProps) => {
    return (<>
        <div className={className}>
            <Image className={styles.round} src={profilePic} alt="247coding" width={160} height={160}/>
            <h1>247Speedtest generator</h1>
        </div>
    </>);
}