import styles from '../styles/social.module.css';
import { SocialIcon} from "react-social-icons";
import React from "react";

export type SocialMediaProps = {[key: string]: string}
export const SocialMedia = (props: SocialMediaProps) => {
    return <div className={styles.socialFlexBox}>
        {Object.keys(props).map(key => {
            return <SocialIcon url={props[key]} key={key} className={styles.itemPadding} target="_blank"/>
        })}
    </div>
}