import Head from 'next/head'
import styles from '@/styles/Home.module.css'
import {SpeedtestGenerator} from "@/components/speedtest-generator";
import React from "react";
import {Header} from "@/components/header";
import {SocialMedia} from "@/components/social";

export default function Home() {
    return (
        <>
            <Head>
                <title>247Gcode is your gateway to 247printertorture!</title>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <main className={styles.main}>
                <div className={styles.content}>
                    <Header className={styles.header}/>
                    <div className={styles.generator}>
                        <SpeedtestGenerator/>
                    </div>
                    <div className={styles.explanation}>
                        <div className="likep">
                            <h3>WARNING: USE AT YOU OWN RISK!</h3>
                        </div>
                        <div className="likep">
                            This speed test generator will create a gcode file to run speed tests for your CoreXY
                            printer&apos;s X-axis with increasing speeds and accelerations. <b>Always have one hand on the
                            Emergency Stop or Power button!</b> If the motors start skipping steps, the toolhead will lose
                            its position and ram into the edges!
                        </div>
                        <div className="likep">
                            Before running the gcode, please open the file and examine the instructions within. Running
                            random code you got off the internet without making sure what it does first is <b>dangerous!</b>
                        </div>
                        <div className="likep">
                            This generator was inspired by the work of <a href="https://www.youtube.com/247printing">247printing</a>,
                            who lives to push consumer FDM printing to its limit. Be sure to give him a follow!
                        </div>
                        <SocialMedia youTube="https://www.youtube.com/247printing" twitter="https://twitter.com/247_printing" instagram="https://www.instagram.com/247printing/"/>
                    </div>
                </div>
                <div className="footer">&copy; 2023 <a href="https://www.twitter.com/DerDackel" target="_blank">Der Dackel</a></div>
            </main>
        </>
    )
}
